﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SlutPrøveAPI.Models
{
    public class NewsRepo
    {
        private readonly NewsDbContext dbContext;

        public NewsRepo(NewsDbContext dbContext)
        {
            this.dbContext = dbContext;
        }
        public IEnumerable<News> GetAllNews()
        {
            return dbContext.News;
        }
        public News GetNewsById(int id)
        {
            News result = new News();
            try
            {
                result = dbContext.News.First(x => x.NewsId == id);
            }
            catch(Exception)
            {
                result = null;
            }
            return result;
        }
        public IEnumerable<News> GetNewsByDate(DateTime startdate, DateTime endDate)
        {
            List<News> news = new List<News>();
            foreach(News n in dbContext.News)
            {
                if(n.CreatedDate >= startdate && n.CreatedDate <= endDate)
                {
                    news.Add(n);
                }
            }
            return news;
        }
        public async Task CreateNews(News news)
        {
            news.CreatedDate = DateTime.Now;
            dbContext.News.Add(news);
            await dbContext.SaveChangesAsync();
        }
        //Denne metode virker ikke ordentligt. Jeg kan ikke få den til at gemme CreatedDate, så jeg kan ikke sikre at en bruger ikke 
        //ændrer den. Jeg har prøvet alverden, men når først jeg kigger på et news object, kan jeg ikke "tracke" et object med samme id.
        //Det betyder at jeg ikke kan få lov til at kigge på det eksisterende element for at gemme CreatedDate. FUCKING PIS.
        public async Task UpdateNews(News news)
        {
            news.UpdatedDate = DateTime.Now;
            dbContext.Update(news);
            await dbContext.SaveChangesAsync();
        }
    }
}
