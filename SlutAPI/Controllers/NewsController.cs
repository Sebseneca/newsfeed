﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SlutPrøveAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SlutPrøveAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class NewsController : ControllerBase
    {
        private readonly NewsRepo repo;

        public NewsController(NewsRepo repo)
        {
            this.repo = repo;
        }
        [HttpGet]
        public IActionResult GetAllNews()
        {
            return Ok(repo.GetAllNews());
        }
        [HttpGet]
        [Route("/news/{id}")]
        public IActionResult GetNewsById(int id)
        {
            var result = repo.GetNewsById(id);
            if(result == null)
            {
                return BadRequest();
            }
            else
            {
                return Ok(result);
            }
        }
        [HttpGet]
        [Route("/news/from/{y1}/{m1}/to/{y2}/{m2}")]
        public IActionResult GetNewsByDate(int y1, int m1, int y2, int m2)
        {
            DateTime startDate = new DateTime(y1, m1, 1);
            DateTime endDate = new DateTime(y2, m2+1, 1);
            return Ok(repo.GetNewsByDate(startDate, endDate));
        }
        [HttpPost]
        [Route("/news/create")]
        public async Task<IActionResult> CreateNews([FromBody] News news)
        {
            await repo.CreateNews(news);
            return Ok(news);
        }
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateNews(int id, [FromBody] News news)
        {
            if(id != news.NewsId)
            {
                return BadRequest();
            }
            else
            {
                await repo.UpdateNews(news);
                return NoContent();
            }
        }
    }
}
