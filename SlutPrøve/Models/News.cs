﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SlutPrøve.Models
{
    public class News
    {
        [JsonProperty("newsId")]
        public int NewsId { get; set; }
        [JsonProperty("author")]
        [Required]
        public string Author { get; set; }
        [JsonProperty("title")]
        [Required]
        [MinLength(10)]
        public string Title { get; set; }
        [JsonProperty("content")]
        [Required]
        [MinLength(50)]
        public string Content { get; set; }
        [JsonProperty("createdDate")]
        public DateTime CreatedDate { get; set; }
        [JsonProperty("updatedDate")]
        public DateTime? UpdatedDate { get; set; }
        [JsonProperty("hashTags")]
        [Required]
        public string HashTags { get; set; }
    }
}
