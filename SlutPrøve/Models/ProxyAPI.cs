﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace SlutPrøve.Models
{
    public class ProxyAPI
    {
        private readonly HttpClient client;

        public ProxyAPI(HttpClient client)
        {
            this.client = client;
        }
        public async Task<List<News>> GetAllNews()
        {
            var response = await client.GetAsync($"{client.BaseAddress}news");
            string result = await response.Content.ReadAsStringAsync();
            var news = JsonConvert.DeserializeObject<List<News>>(result);
            return news;
        }
        public async Task<News> GetNewsById(int id)
        {
            var response = await client.GetAsync($"{client.BaseAddress}news/{id}");
            string result = await response.Content.ReadAsStringAsync();
            var news = JsonConvert.DeserializeObject<News>(result);
            return news;
        }
        public async Task<List<News>> GetNewsByDate(int y1, int m1, int y2, int m2)
        {
            var response = await client.GetAsync($"{client.BaseAddress}news/from/{y1}/{m1}/to/{y2}/{m2}");
            string result = await response.Content.ReadAsStringAsync();
            var news = JsonConvert.DeserializeObject<List<News>>(result);
            return news;
        }
        public async Task CreateNews(News n)
        {
            var output = JsonConvert.SerializeObject(n);
            StringContent content = new StringContent(output);
            await client.PostAsync($"{client.BaseAddress}news/create", content);
        }
    }
}
