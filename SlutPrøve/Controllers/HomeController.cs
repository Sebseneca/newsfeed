﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SlutPrøve.Models;
using SlutPrøve.Models.Users;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace SlutPrøve.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ProxyAPI proxy;

        public HomeController(ILogger<HomeController> logger, ProxyAPI proxy)
        {
            _logger = logger;
            this.proxy = proxy;
        }

        public IActionResult Index()
        {
            return View();
        }
        [HttpGet("/news")]
        public async Task<IActionResult> AllNews()
        {
            var news = await proxy.GetAllNews();
            return View(news);
        }
        [HttpGet]
        public async Task<IActionResult> ViewNews(int id)
        {
            var news = await proxy.GetNewsById(id);
            return View(news);
        }
        [HttpGet]
        [Authorize]
        [Route("/news/from/{y1}/{m1}/to/{y2}/{m2}")]
        public async Task<IActionResult> NewsByDate(int y1, int m1, int y2, int m2)
        {
            var news = await proxy.GetNewsByDate(y1, m1, y2, m2);
            return View(news);
        }
        [HttpGet]
        [Authorize]
        [Route("/news/create")]
        public IActionResult CreateNews()
        {
            return View();
        }
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> CreateNews(News news)
        {
            await proxy.CreateNews(news);
            return RedirectToAction("AllNews", "Home");
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
