# NewsFeed

Et simpelt projekt jeg arbejdede på for at gennemgå forskellige elementer af programmeringspensum for 3. semester.
Det består af et MVC projekt som dækker en webapplikation og en API der håndtere data for siden. 
Selve siden består af en liste med nyheder og et login system. Når man har oprettet en bruger og er logget ind, har man mulighed for at oprette en ny nyhed, som bliver tilføjet til listen af nyheder.
Login systemet er implementeret med IdentityDbContext.
Frontend elementer er implementeret i Razor views.

Lad det være noteret at dette var et projekt jeg arbejdede på under eksamensforberedelse, og det ikke just er færdigimplementeret. Funktionaliteten for at oprette en nyhed via siden virker pt ikke. 
